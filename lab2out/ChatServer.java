package lab2out;

import ocsf.server.AbstractServer;
import ocsf.server.ConnectionToClient;

import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import javax.swing.JFrame;
import javax.swing.JLabel;
import java.io.IOException;
import javax.swing.UIManager;

public class ChatServer extends AbstractServer {
    
    private JTextArea log; //Corresponds to JTextArea of ServerGUI
    private JLabel status; //Corresponds to the JLabel of ServerGUI
    
	public ChatServer() {
		super(12345);
		super.setTimeout(500);
	}

	@Override
	protected void handleMessageFromClient(Object arg0, ConnectionToClient arg1) {
		System.out.println("Message from Client Received");

	}
	
	public void listeningException(Throwable exception) {
		this.log.append(exception.toString() + "\n");
        this.status.setForeground(Color.red);
        this.status.setText("Exception Occurred when Listening");
	}
	
	public void serverStarted() {
		this.log.append("Server Started\n");
        this.status.setForeground(Color.green);
        this.status.setText("Listening");
	}
    
    public void serverStopped()
    {
        this.log.append("Server Stopped Accepting New Clients - Press Listen to Start Accepting New Clients\n");
        this.status.setForeground(Color.red);
        this.status.setText("Stopped");
    }
    
    public void serverClosed()
    {
        this.log.append("Server and all current clients are closed - Press Listen to Restart\n");
        this.status.setForeground(Color.red);
        this.status.setText("Close");
    }
    
    public void setLog(JTextArea log)
    {
        this.log = log;
    }
    public void setStatus(JLabel status)
    {
        this.status = status;
    }
    
    public void clientConnected(ConnectionToClient client)
    {
        this.log.append("Client Connected");
    }

}
